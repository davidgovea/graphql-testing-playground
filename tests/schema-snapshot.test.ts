import fs from 'fs';
import path from 'path';

test('schema matches snapshot', async () => {
  const schema = fs.readFileSync(path.join(__dirname,  '../schema.gql')).toString();
  expect(schema).toMatchSnapshot();
});