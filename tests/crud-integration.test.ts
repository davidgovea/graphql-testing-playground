import { gql } from 'apollo-server';
import {
  ApolloServerTestClient,
  createTestClient,
} from 'apollo-server-testing';
import debug from 'debug';
import { file } from 'tmp-promise';

import { createServer, seedOrgs } from '../src/server';
import { Organization } from '../src/entities/organization';

const log = debug('integration-test');

// Clean up tmp database files after all tests
const cleanupFunctions: Array<() => void> = [];
afterAll(() => cleanupFunctions.forEach((fn) => fn()));

let testClient: ApolloServerTestClient;
beforeAll(async () => {
  const tmpFile = await file();
  const { path, cleanup } = tmpFile;
  cleanupFunctions.push(cleanup);

  const server = await createServer({
    type: 'sqlite',
    database: path,
    synchronize: true,
    entities: ['src/entities/*.ts'],
    dropSchema: true,
  });

  // Seeds 3 organizations
  await seedOrgs();

  testClient = createTestClient(server as any);
});

describe('CRUD for Locations', () => {
  let locationId: string;

  test('Create Location', async () => {
    const { mutate } = testClient;
    const {
      data: { locationSave: newLocation },
    } = await mutate({
      mutation: gql`
        mutation($organizationId: String!, $data: LocationInput!) {
          locationSave(organizationId: $organizationId, data: $data) {
            ...locationFields
          }
        }
        ${locationFields}
      `,
      variables: {
        organizationId: '2',
        data: {
          name: 'White House',
          address: '1600 Pennsylvania Ave NW, Washington, DC 20500',
        },
      },
    });

    log('created location: %O', newLocation);
    locationId = newLocation.id;

    expect(newLocation.name).toBe('White House');
    expect(newLocation.createdAt).toBeTruthy();
    expect(newLocation.createdAt).toEqual(newLocation.updatedAt);
  });

  test('Read Location', async () => {
    const { query } = testClient;
    const {
      data: { location },
    } = await query({
      query: gql`
        query($id: String!) {
          location(id: $id) {
            ...locationFields
          }
        }
        ${locationFields}
      `,
      variables: {
        id: locationId,
      },
    });

    log('read location: %O', location);

    expect(location.name).toBe('White House');
  });

  test('Update Location', async () => {
    const { mutate } = testClient;
    const {
      data: { locationSave: updatedLocation },
    } = await mutate({
      mutation: gql`
        mutation($organizationId: String!, $data: LocationInput!) {
          locationSave(organizationId: $organizationId, data: $data) {
            ...locationFields
          }
        }
        ${locationFields}
      `,
      variables: {
        organizationId: '2',
        data: {
          id: locationId,
          name: 'THE White House',
          address: '1600 Pennsylvania Ave NW, Washington, DC 20500',
        },
      },
    });

    log('updated location: %O', updatedLocation);

    expect(updatedLocation.name).toBe('THE White House');
    expect(updatedLocation.createdAt).not.toEqual(updatedLocation.updatedAt);
  });

  test('Delete Location', async () => {
    const { mutate } = testClient;

    const {
      data: { locationSave: newLocation },
    } = await mutate({
      mutation: gql`
        mutation($organizationId: String!, $data: LocationInput!) {
          locationSave(organizationId: $organizationId, data: $data) {
            id
          }
        }
      `,
      variables: {
        organizationId: '2',
        data: {
          name: 'Google',
          address: '1600 Amphitheatre Parkway, Mountain View, CA 94043',
        },
      },
    });

    const {
      data: { locationDelete: deleted },
    } = await mutate({
      mutation: gql`
        mutation($id: String!) {
          locationDelete(id: $id)
        }
      `,
      variables: {
        id: newLocation.id,
      },
    });

    log('deleted location: %O', deleted);

    expect(deleted).toBe(true);
  });
});

describe('CRUD for Events', () => {
  let eventId: string;

  test('Create Event', async () => {
    const { mutate } = testClient;
    const {
      data: { eventSave: newEvent },
    } = await mutate({
      mutation: gql`
        mutation($organizationId: String!, $data: EventInput!) {
          eventSave(organizationId: $organizationId, data: $data) {
            ...eventFields
          }
        }
        ${eventFields}
      `,
      variables: {
        organizationId: '2',
        data: {
          name: 'New Event',
          date: '2019-08-16T05:00:00.000Z',
        },
      },
    });

    log('created event: %O', newEvent);
    eventId = newEvent.id;

    expect(newEvent.name).toBe('New Event');
    expect(newEvent.createdAt).toBeTruthy();
    expect(newEvent.createdAt).toEqual(newEvent.updatedAt);
  });

  test('Read Event', async () => {
    const { query } = testClient;
    const {
      data: { event },
    } = await query({
      query: gql`
        query($id: String!) {
          event(id: $id) {
            ...eventFields
          }
        }
        ${eventFields}
      `,
      variables: {
        id: eventId,
      },
    });

    log('read event: %O', event);

    expect(event.name).toBe('New Event');
  });

  test('Update Event', async () => {
    const { mutate } = testClient;
    const {
      data: { eventSave: updatedEvent },
    } = await mutate({
      mutation: gql`
        mutation($organizationId: String!, $data: EventInput!) {
          eventSave(organizationId: $organizationId, data: $data) {
            ...eventFields
          }
        }
        ${eventFields}
      `,
      variables: {
        organizationId: '2',
        data: {
          id: eventId,
          name: 'Updated Event',
          date: '2019-09-16T05:00:00.000Z',
        },
      },
    });

    log('updated event: %O', updatedEvent);

    expect(updatedEvent.name).toBe('Updated Event');
    expect(updatedEvent.createdAt).not.toEqual(updatedEvent.updatedAt);
  });

  test('Delete Event', async () => {
    const { mutate } = testClient;

    const {
      data: { eventSave: newEvent },
    } = await mutate({
      mutation: gql`
        mutation($organizationId: String!, $data: EventInput!) {
          eventSave(organizationId: $organizationId, data: $data) {
            id
          }
        }
      `,
      variables: {
        organizationId: '2',
        data: {
          name: 'Event to be deleted',
          date: '2019-08-16T05:00:00.000Z',
        },
      },
    });

    const {
      data: { eventDelete: deleted },
    } = await mutate({
      mutation: gql`
        mutation($id: String!) {
          eventDelete(id: $id)
        }
      `,
      variables: {
        id: newEvent.id,
      },
    });

    log('deleted event: %O', deleted);

    expect(deleted).toBe(true);
  });
});

describe('Find Events and Locations, by Organization', () => {
  test('Find events/locations for one Organization', async () => {
    const { query } = testClient;
    const {
      data: { organization },
    } = await query({
      query: gql`
        query($id: String!) {
          organization(id: $id) {
            locations {
              ...locationFields
            }
            events {
              ...eventFields
            }
          }
        }
        ${locationFields}
        ${eventFields}
      `,
      variables: {
        id: '2',
      },
    });

    log('found events/locations for org 2: %O', organization);
    expect(organization.events.length).toBeGreaterThan(0);
    expect(organization.locations.length).toBeGreaterThan(0);
  });
});

describe('Find Organizations, by Location(s) / Event(s)', () => {
  test('Query for Organizations of all locations/events', async () => {
    const { query } = testClient;
    const {data} = await query({
      query: gql`
        query($locationIds: [ID!], $eventIds: [ID!]) {
          locations(ids: $locationIds) {
            id
            organization {
              id
              name
            }
          }
          events(ids: $eventIds) {
            id
            organization {
              id
              name
            }
          }
        }
      `,
      variables: {
        eventIds: null, // Could query by specific ids here
        locationIds: null,
      },
    });

    // Collect and dedupe organizations found in query
    const orgMap = [...data.locations, ...data.events].reduce((map: Map<string, Organization>, i) => {
      const orgId = i.organization && i.organization.id;
      if (orgId) {
        map.set(orgId, i.organization);
      }
      return map;
    }, new Map());

    const foundOrgs = [...orgMap.values()];

    log('found orgs via ALL locations/events: %O', foundOrgs);
    expect(foundOrgs.length).toBeGreaterThan(0);
  });
});

// Would rather load these from `sample-queries.graphql`,
//  but I was spending too much time with AST transforms. Copy-paste for now.
const eventFields = gql`
  fragment eventFields on Event {
    id
    name
    date
    description
    createdAt
    updatedAt
  }
`;
const locationFields = gql`
  fragment locationFields on Location {
    id
    name
    address
    latitude
    longitude
    createdAt
    updatedAt
  }
`;
