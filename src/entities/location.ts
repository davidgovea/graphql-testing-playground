import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { ObjectType, Field, ID } from 'type-graphql';
import { Organization } from './organization';

@Entity()
@ObjectType()
export class Location {
  @PrimaryGeneratedColumn()
  @Field((type) => ID)
  readonly id: string;

  @Column()
  @Field()
  name: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  address: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  latitude: Number;

  @Column({ nullable: true })
  @Field({ nullable: true })
  longitude: Number;

  @Column()
  @Field()
  createdAt: Date;

  @Column()
  @Field()
  updatedAt: Date;

  @Field(type => Organization)
  @ManyToOne((type) => Organization, (organization) => organization.events)
  organization: Organization;

  @Field(type => ID)
  @Column({ nullable: true })
  organizationId: string;
}
