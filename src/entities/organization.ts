import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';
import { Location } from './location';
import { Event } from './event';

@Entity()
@ObjectType()
export class Organization {
  @PrimaryGeneratedColumn()
  @Field((type) => ID)
  readonly id: string;

  @Column()
  @Field()
  name: string;

  @Column()
  @Field()
  createdAt: Date;

  @Column()
  @Field()
  updatedAt: Date;

  @Field(type => [Location])
  @OneToMany((type) => Location, (location) => location.organization)
  locations: Location[];

  @Field(type => [Event])
  @OneToMany((type) => Event, (event) => event.organization)
  events: Event[];
}
