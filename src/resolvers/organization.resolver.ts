import { Arg, Mutation, Query, Resolver, ID } from 'type-graphql';
import { Repository } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';

import { Organization } from '../entities/organization';
import { readOne, readMany, save, deleter } from './crud-routes';

const LOAD_RELATIONS = ['events', 'locations'];
@Resolver((of) => Organization)
export class OrganizationResolver {
  constructor(
    @InjectRepository(Organization)
    private readonly organizationRepository: Repository<Organization>
  ) {}

  @Query((returns) => Organization)
  organization(@Arg('id') id: string) {
    return readOne(id, this.organizationRepository, {
      relations: LOAD_RELATIONS,
    });
  }

  @Query((returns) => [Organization])
  organizations(@Arg('ids', (type) => [ID], { nullable: true }) ids: string[]) {
    return readMany(ids, this.organizationRepository, {
      relations: LOAD_RELATIONS,
    });
  }

  // Disabled on purpose -- not part of design spec
  // @Mutation((returns) => Organization)
  organizationSave(
    /* @Arg('id', { nullable: true }) */ id: string,
    /* @Arg('name') */ name: string
  ) {
    return save(
      {
        name,
        id,
      },
      this.organizationRepository,
      { relations: LOAD_RELATIONS }
    );
  }

  // Disabled on purpose -- not part of design spec
  // @Mutation((returns) => Boolean)
  // organizationDelete(/* @Arg('id') */ id: string) {
  //   return deleter(id, this.organizationRepository);
  // }
}

export default OrganizationResolver;
