import fetch from 'cross-fetch';
import { Arg, ID, Mutation, Query, Resolver } from 'type-graphql';
import { Repository } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';

import { Location } from '../entities/location';
import { LocationInput } from './types/location-input';
import { deleter, save, readMany, readOne } from './crud-routes';

const GOOGLE_MAPS_API_KEY = 'AIzaSyCzRrnhBOYxTQ2FNs5Pt_vnkgtGF0scIC8';
const GOOGLE_GEOCODE_API = `https://maps.googleapis.com/maps/api/geocode/json?key=${GOOGLE_MAPS_API_KEY}`;

// Quick type for the portion of the geocode data we will use.
// Lighter than bringing in the node client sdk.
interface GoogleGeocodeResponse {
  results: Array<{
    geometry: {
      location: {
        lat: number;
        lng: number;
      };
    };
  }>;
}

const LOAD_RELATIONS = ['organization'];

@Resolver((of) => Location)
export class LocationResolver {
  constructor(
    @InjectRepository(Location)
    private readonly locationRepository: Repository<Location>
  ) {}

  @Query((returns) => Location)
  location(@Arg('id') id: string) {
    return readOne(id, this.locationRepository, {
      relations: LOAD_RELATIONS,
    });
  }

  @Query((returns) => [Location])
  locations(@Arg('ids', (type) => [ID], { nullable: true }) ids: string[]) {
    return readMany(ids, this.locationRepository, {
      relations: LOAD_RELATIONS,
    });
  }

  @Mutation((returns) => Location)
  async locationSave(
    @Arg('organizationId') organizationId: string,
    @Arg('data') newLocationData: LocationInput
  ) {
    let augmentedData = {};

    const { address } = newLocationData;
    const isCreate = !newLocationData.id;
    const isMissingLatOrLng =
      !newLocationData.latitude || !newLocationData.longitude;
    if (isCreate && address && isMissingLatOrLng) {
      try {
        const geocodeResponse = await fetch(
          `${GOOGLE_GEOCODE_API}&address=${address}`
        );
        if (!geocodeResponse.ok) {
          throw new Error(geocodeResponse.statusText);
        }
        const geocodeData: GoogleGeocodeResponse = await geocodeResponse.json();
        const [firstResult] = geocodeData.results;
        const { lat, lng } = firstResult.geometry.location;
        augmentedData = {
          ...augmentedData,
          latitude: lat,
          longitude: lng,
        };
      } catch (e) {
        // Allow failure: this is an optional feature
        // Would want to log this. A timeout would be good too
      }
    }

    return save(
      {
        organizationId,
        ...newLocationData,
        ...augmentedData,
      },
      this.locationRepository,
      { relations: LOAD_RELATIONS }
    );
  }

  @Mutation((returns) => Boolean)
  locationDelete(@Arg('id') id: string) {
    return deleter(id, this.locationRepository);
  }
}

export default LocationResolver;
