import { Repository } from 'typeorm';

export async function save(data: any, repository: Repository<any>, opts = {}) {
  const now = new Date();
  const isCreate = !data.id;

  const augmentedData = {
    ...data,
    ...(isCreate && { createdAt: now }),
    updatedAt: now,
  };

  if (!isCreate) {
    await repository.update(data.id, Object.fromEntries(
      Object.entries(augmentedData).filter(([k, v]) => !!v || v === false)
    ) as any);
  } else {
    await repository.save(augmentedData);
  }
  return repository.findOne(augmentedData.id, opts);
}

export function readOne(id: string, repository: Repository<any>, opts = {}) {
  return repository.findOne(id, opts);
}

export function readMany(
  ids: string[] | null,
  repository: Repository<any>,
  opts = {}
) {
  if (ids && ids.length) {
    return repository.findByIds(ids, opts);
  }
  return repository.find(opts);
}

export async function deleter(id: string, repository: Repository<any>) {
  try {
    const existing = await repository.findOne(id);
    if (!existing) {
      throw new Error('Does not exist');
    }
    await repository.delete(id);
    return true;
  } catch {
    return false;
  }
}
