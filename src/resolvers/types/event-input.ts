import { InputType, Field } from "type-graphql";
import { Event } from "../../entities/event";

@InputType()
export class EventInput implements Partial<Event> {
  @Field({ nullable: true })
  id?: string;

  @Field()
  name: string;

  @Field()
  date: Date;

  @Field({ nullable: true })
  description?: string;
}