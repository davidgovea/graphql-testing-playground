import { InputType, Field } from "type-graphql";
import { Event } from "../../entities/event";

@InputType()
export class LocationInput implements Partial<Event> {
  @Field({ nullable: true })
  id?: string;

  @Field()
  name: string;

  @Field({ nullable: true })
  address?: string;

  @Field({ nullable: true })
  latitude?: number;

  @Field({ nullable: true })
  longitude?: number;
}