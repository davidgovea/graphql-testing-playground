import { Arg, ID, Mutation, Query, Resolver } from 'type-graphql';
import { Repository } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';

import { Event } from '../entities/event';
import { deleter, readMany, readOne, save } from './crud-routes';
import { EventInput } from './types/event-input';

const LOAD_RELATIONS = ['organization'];

@Resolver((of) => Event)
export class EventResolver {
  constructor(
    @InjectRepository(Event)
    private readonly eventRepository: Repository<Event>
  ) {}

  @Query((returns) => Event)
  event(@Arg('id') id: string) {
    return readOne(id, this.eventRepository, {
      relations: LOAD_RELATIONS
    });
  }

  @Query((returns) => [Event])
  events(@Arg('ids', (type) => [ID], { nullable: true }) ids: string[]) {
    return readMany(ids, this.eventRepository, {
      relations: LOAD_RELATIONS
    });
  }

  @Mutation((returns) => Event)
  eventSave(
    @Arg('organizationId') organizationId: string,
    @Arg('data') newEventData: EventInput
  ) {
    return save(
      {
        organizationId,
        ...newEventData,
      },
      this.eventRepository,
      { relations: LOAD_RELATIONS }
    );
  }

  @Mutation((returns) => Boolean)
  async eventDelete(@Arg('id') id: string) {
    return deleter(id, this.eventRepository);
  }
}
export default EventResolver;
