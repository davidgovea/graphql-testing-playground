import "reflect-metadata";
import { buildSchema } from "type-graphql";
import * as TypeORM from "typeorm";
import { Container } from "typedi";
import { ApolloServer } from "apollo-server";
import OrganizationResolver from "./resolvers/organization.resolver";
import { Organization } from "./entities/organization";

TypeORM.useContainer(Container);

const PORT = process.env.PORT || 4000;

export async function seedOrgs() {
  const orgRes = new OrganizationResolver(TypeORM.getRepository(Organization));
  for (const name of ["Org1", "Org2", "Org3"]) {
    await orgRes.organizationSave(undefined, name);
  }
}

export async function createServer(ormOpts?: TypeORM.ConnectionOptions) {
  await TypeORM.createConnection();

  const schema = await buildSchema({
    resolvers: [__dirname + "/resolvers/*.ts"],
    emitSchemaFile: true,
    container: Container,
    validate: false,
  });

  return new ApolloServer({
    schema,
    playground: true,
  });
}

export async function bootstrap() {
  const server = await createServer();
  const { url } = await server.listen(PORT);
  console.log(`Server is running, GraphQL Playground available at ${url}`);
  await seedOrgs();
}

if (!module.parent) {
  bootstrap()
}
